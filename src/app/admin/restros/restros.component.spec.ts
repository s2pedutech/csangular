import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestrosComponent } from './restros.component';

describe('RestrosComponent', () => {
  let component: RestrosComponent;
  let fixture: ComponentFixture<RestrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
