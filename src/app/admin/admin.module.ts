import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { RestrosComponent } from './restros/restros.component';
import { UsersComponent } from './users/users.component';
import { OrdersComponent } from './orders/orders.component';

@NgModule({
  declarations: [HomeComponent, RestrosComponent, UsersComponent, OrdersComponent],
  imports: [
    CommonModule
  ]
})
export class AdminModule { }
