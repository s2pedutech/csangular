import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  selectedName:any = {}
  students:any = [{
    "name":"Sudesh", "rno":1
  },{
    "name":"Mehul", "rno":2
  },{
    "name":"Dipti", "rno": 3
  }];

  selectName(o)
  {
    console.log(o);
    this.selectedName = o;
  }

  display(s)
  {
    this.selectedName = s;
  }
  constructor() { }

  ngOnInit() {
  }

}
