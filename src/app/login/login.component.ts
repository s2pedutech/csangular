import { Component, OnInit } from '@angular/core';
import {LoginService} from '../login.service';
import {CountriesService} from '../countries.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Router} from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email : new FormControl(),
    pwd: new FormControl()
  });
  countries:any = [];
  products:any = [];
  constructor(private lSer:LoginService, private router:Router, private cSer:CountriesService) { }


  getAllCountries()
  {
    this.cSer.getAllCountries().subscribe(success => {
      console.log(success);
      this.countries = success;
    }, error => {
      console.log(error);
    });
  }
  ngOnInit() {
    // this.getAllCountries();    
    
  }

  reset()
  {
    this.loginForm.reset();
  }
  login()
  {
    let u = this.lSer.login(this.loginForm.value);
    if(!u)
    {
      alert('Login Failed');
    }
    else
    {
      console.log(u);
      let role = u.role;
      console.log(role);
      if(role == 'user')
        this.router.navigateByUrl('/user/home');
      else
        if(role == 'admin')
        {
          this.router.navigateByUrl('/admin/home');
        }
        else
        this.router.navigateByUrl('/restro/home');
    }

    
      // send the name

      // this.lSer.addProduct({"ProductName": "Honda"}).subscribe(success => {
      //   console.log(success);
      //   this.lSer.getAllProducts().subscribe(success => {
      //     console.log(success);
      //     this.products = success;
      //   }, error => {
      //     console.log(error);
      //   });


      // }, error => {
      //   console.log(error);
      // });

  }

}
