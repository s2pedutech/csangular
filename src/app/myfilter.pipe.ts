import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myfilter'
})
export class MyfilterPipe implements PipeTransform {

  transform(arr1, term: string): any {
    let t:string[] = arr1;
    arr1 = arr1.filter(x => {
      if(x.name.toLowerCase().startsWith(term))
        return x;
    });

    return arr1;
  }

}
