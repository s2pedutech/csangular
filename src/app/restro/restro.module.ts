import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { MenuComponent } from './menu/menu.component';
import { ProfileComponent } from './profile/profile.component';
import { OrdersComponent } from './orders/orders.component';

import {RouterModule,Routes} from '@angular/router';

var routes:Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'menu',
    component: MenuComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'orders',
    component: OrdersComponent
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }
];



@NgModule({
  declarations: [HomeComponent, MenuComponent, ProfileComponent, OrdersComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ]
})
export class RestroModule { }
