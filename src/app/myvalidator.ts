import { AbstractControl } from '@angular/forms';
export class Myvalidator  {

    static myvalidator(x: AbstractControl) {

        if(x.value > 200)
        {
            return { 'costly':true};
        }

        return null;
    }
    
}
