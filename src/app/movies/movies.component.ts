import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  moviename:string = "";
  movies:any = [
    {
      "name":"DDLJ", "actor": "SRK"
    },{
      "name":"RHTDM","actor":"RM"
    },{
      "name":"YJHD","actor":"RRK"
    }
  ];

  sortByName()
  {

    console.log("In sorting");
    this.displayMovies.sort((a,b) => {
      if(a.name < b.name)
      return 1;
      else
      return -1;
    });
  }

  sortByActor()
  {
    console.log("In actor sorting");
    this.displayMovies.sort((a,b) => {
      if(a.actor < b.actor)
        return 1;
      else
        return -1;
    });
  }

  displayMovies:any = [];

  filterItems()
  {

    console.log(this.moviename);
    
    this.displayMovies = this.movies.filter(x => {
      if(x.name.toLowerCase().includes(this.moviename.toLowerCase()) || x.actor.toLowerCase().includes(this.moviename.toLowerCase()))
        return x;
    });
  }
  delMovie(m)
  {
    for(let j=0; j<this.movies.length; j++)
    {
      if(this.movies[j].name == m.name)
      {
        this.movies.splice(j,1);
      }
    }
  }

  addMovie()
  {
    this.movies.push({"name":"PK", "actor":"AK"});
  }
  constructor() { }

  ngOnInit() {

    this.displayMovies = this.movies;
  }

}
