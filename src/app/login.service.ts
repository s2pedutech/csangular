import { Injectable, Inject } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  allusers = [
    { "email":"user", "pwd":"user", "role":"user"},
    { "email":"admin", "pwd":"admin", "role":"admin"},
    { "email":"restro", "pwd":"restro", "role":"restro"}
  ];
  currentUser: any = null;
  constructor(private http: HttpClient, @Inject(LOCAL_STORAGE) private storage: WebStorageService,
  private router:Router) { }

  login(u)
  {
    for(let i=0; i < this.allusers.length; i++)
    {
      if(this.allusers[i].email == u.email && this.allusers[i].pwd == u.pwd)
      {
        this.currentUser = this.allusers[i];
        this.storage.set("user",this.allusers[i]);
        return this.allusers[i];
      }
    }

    return null;

  }
  logout()
  {
    this.storage.remove("user");
    this.currentUser = null;
    this.router.navigateByUrl('/login');
  }

  addProduct(p)
  {
    console.log(p);
    return this.http.post("http://localhost:3000/mahindra/productdetails", p);
  }

  getAllProducts()
  {
    return this.http.get("http://localhost:3000/mahindra/productdetails");
  }

  getCurrentUser()
  {
    return this.storage.get("user");
    // return this.currentUser;
  }


  usignup()
  {

  }

  rsignup()
  {

  }
}
