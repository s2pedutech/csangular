import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule,Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { StudentComponent } from './student/student.component';
import { MoviesComponent } from './movies/movies.component';
import { SortmoviePipe } from './sortmovie.pipe';
import { MyupperPipe } from './myupper.pipe';
import { MyfilterPipe } from './myfilter.pipe';
import { LoginComponent } from './login/login.component';
import { UsignupComponent } from './usignup/usignup.component';
import { RsignupComponent } from './rsignup/rsignup.component';
import { ZomatohomeComponent } from './zomatohome/zomatohome.component';
import {HttpClientModule} from '@angular/common/http';
import {AuthguardService} from './authguard.service';
import {StorageServiceModule} from 'angular-webstorage-service';

var routes:Routes = [
  {
    path : 'home',
    component: ZomatohomeComponent
  },{
    path: 'students',
    component: StudentComponent
  },{
    path: 'movies',
    component: MoviesComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },{
    path: 'usignup',
    component: UsignupComponent
  },
  {
    path: 'rsignup',
    component: RsignupComponent,
   
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'restro',
    loadChildren: './restro/restro.module#RestroModule'
  },
  {
    path: 'user',
    loadChildren: './user/user.module#UserModule',
   canActivate: [AuthguardService]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StudentComponent,
    MoviesComponent,
    SortmoviePipe,
    MyupperPipe,
    MyfilterPipe,
    LoginComponent,
    UsignupComponent,
    RsignupComponent,
    ZomatohomeComponent,
    
  ],
  imports: [
    BrowserModule,FormsModule,ReactiveFormsModule,RouterModule.forRoot(routes),HttpClientModule,StorageServiceModule
  ],
  providers: [AuthguardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
