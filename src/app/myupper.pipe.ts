import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myupper'
})
export class MyupperPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let str:string = "";
    
    return value.toUpperCase();
  }

}
