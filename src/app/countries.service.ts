import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  baseUrl = "https://restcountries.eu/rest/v2/all";
  getAllCountries()
  {
    return this.http.get(this.baseUrl);
  }
  constructor(private http:HttpClient) { 

  }
}
