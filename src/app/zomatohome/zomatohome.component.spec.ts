import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZomatohomeComponent } from './zomatohome.component';

describe('ZomatohomeComponent', () => {
  let component: ZomatohomeComponent;
  let fixture: ComponentFixture<ZomatohomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZomatohomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZomatohomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
