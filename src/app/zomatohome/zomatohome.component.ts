import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';

@Component({
  selector: 'app-zomatohome',
  templateUrl: './zomatohome.component.html',
  styleUrls: ['./zomatohome.component.css']
})
export class ZomatohomeComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  signupUser(){
    this.router.navigateByUrl('usignup');
  }
  signupRestro(){
    this.router.navigateByUrl('rsignup');
  }
}
