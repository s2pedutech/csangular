import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { LoginService } from './login.service';
import {Router} from '@angular/router';

@Injectable()
export class AuthguardService implements CanActivate{
  

  constructor(private lSer:LoginService, private router:Router) { }

  canActivate() : boolean
  {
    if(this.lSer.getCurrentUser())
      return true;
    
    this.router.navigateByUrl('/login');
      // return false;
  }
}
