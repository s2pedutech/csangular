import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Myvalidator} from '../myvalidator';
import {Pricevalidator} from '../pricevalidator';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  foodForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    name: new FormControl('Rice', [Validators.required, Validators.minLength(6)]),
    price: new FormControl('', [Validators.required, Pricevalidator.chkPrice]),
    foods: new FormControl(),
    currdate : new FormControl(new Date())
  });

  mydate = new Date();
  searchTerm:string;
  foods = [
    {
      "name":"dosa",
      "price":50,
      "img":"assets/img/flute.jpg"
    },
    {
      "name":"noodles",
      "price": 70,
      "img" : "assets/img/flute.jpg"
    },
    {
      "name":"pav bhaji",
      "price": 169,
      "img":"assets/img/flute.jpg"
    }
  ];
  constructor() {
    console.log("In home constructor");
   }

   addFood()
   {
     console.log(this.foodForm.value);
     this.foods.push(this.foodForm.value);
   }

   reset()
   {
     this.foodForm.reset();
   }
  ngOnInit() {
    console.log("In on init");
  }

}
