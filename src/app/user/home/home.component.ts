import { Component, OnInit, OnChanges, DoCheck, AfterContentChecked, AfterContentInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import {LoginService} from '../../login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, DoCheck, AfterContentChecked, AfterContentInit, AfterViewInit, AfterViewChecked {

  img = "assets/img/flute.jpg";
  restroname="behrouz";
  description = "Authentic Biryani Restaurent"
  cardtitle:string;
  currentUser:any = null;
  color = "green";

  restros:any = [
    { "name":"Behrouz", "img":"assets/img/flute.jpg", "type":"Chinese", "color":"green", "description":"D1"},
    { "name":"Behrouz1", "img":"assets/img/flute.jpg", "type":"Chinese", "color":"gray", "description":"D2"},
    { "name":"Behrouz2", "img":"assets/img/flute.jpg", "type":"Chinese", "color":"blue", "description":"D3"},
    { "name":"Behrouz3", "img":"assets/img/flute.jpg", "type":"Chinese", "color":"red", "description":"D4"}
  ];
  constructor(private lSer:LoginService, private router:Router) { }

  ngOnInit() {
    console.log("In home => on init ")
    this.currentUser = this.lSer.getCurrentUser();
  }
  
  ngDoCheck()
  {
    console.log(" In do check");
  }

  ngAfterContentInit()
  {
    console.log(" In after content init");
  }
  ngAfterContentChecked()
  {
    console.log(" In after content checked");
  }
  ngAfterViewInit()
  {
    console.log(" In after View Init");
  }

  ngAfterViewChecked()
  {
    console.log(" In after View Checked");
  }

  gotoDetails(r)
  {
    let route = "/user/rdetails";
    // route += "name=" + r.name + "&color="+ r.color;
    // console.log(route);
    // this.router.navigateByUrl(route);
    let obj:any = {};
    obj.name = r.name;
    obj.color = r.color;
    this.router.navigate([route], {
      queryParams: obj
    });
  }
  changeColor()
  {
    if(this.restros[3].name == "Behrouz3")
      this.restros[3].name = "BB1";
    else
    this.restros[3].name = "Behrouz3";
  }
  displayTitle(event)
  {
    this.cardtitle = event;
  }
  logout()
  {
    this.lSer.logout();
  }
}
