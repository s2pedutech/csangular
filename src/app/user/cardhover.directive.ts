import { Directive, ElementRef, Input, OnInit } from '@angular/core';


@Directive({
  selector: '[appCardhover]'
})
export class CardhoverDirective implements OnInit{

  @Input() appCardhover;
  constructor(private el:ElementRef) {
    
    
   }
   ngOnInit()
   {
    this.el.nativeElement.style.backgroundColor = this.appCardhover;
    // this.el.nativeElement.style.backgroundColor = "red";
   }

}
