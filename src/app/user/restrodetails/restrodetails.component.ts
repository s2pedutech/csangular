import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-restrodetails',
  templateUrl: './restrodetails.component.html',
  styleUrls: ['./restrodetails.component.css']
})
export class RestrodetailsComponent implements OnInit {

  name:string;
  constructor(private act:ActivatedRoute) { }

  ngOnInit() {

    this.act.queryParams.subscribe(par => {
      console.log(par);
      // this.name = par.id;
    })
  }

}
