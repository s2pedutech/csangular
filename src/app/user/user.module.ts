import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { CartComponent } from './cart/cart.component';
import { RestrodetailsComponent } from './restrodetails/restrodetails.component';
import {RouterModule, Routes} from '@angular/router';
import { CardComponent } from './card/card.component';
import { CardhoverDirective } from './cardhover.directive';
 

var routes : Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'cart',
    component: CartComponent
  },
  {
    path: 'rdetails',
    component: RestrodetailsComponent
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [HomeComponent, ProfileComponent, CartComponent, RestrodetailsComponent, CardComponent, CardhoverDirective],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ]
})
export class UserModule { }
