import { Component, OnInit, Input, Output,EventEmitter, OnChanges } from '@angular/core';
// import { EventEmitter } from 'events';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit, OnChanges {
  

  @Input() myimg:string;
  @Input() title:string;
  @Input() desc:string;

  @Output() sendBack = new EventEmitter();
  constructor() { }

  changeToUpper()
  {
    this.sendBack.emit(this.title.toUpperCase());
  }
  ngOnInit() {
    console.log(" card on init");
  }

  ngOnChanges()
  {
    console.log("Changed");
  }

}
